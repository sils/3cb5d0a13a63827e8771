        did_nothing = True
        yielded_results = False
        (sections,
         local_bears,
         global_bears,
         targets) = gather_configuration(interactor.acquire_settings,
                                         log_printer)

        if bool(sections["default"].get("show_bears", "False")):
            show_bears(local_bears,
                       global_bears,
                       interactor.show_bears)
            did_nothing = False
        else:
            section_results = execute_enabled_sections(sections,
                                                       targets,
                                                       global_bears,
                                                       local_bears,
                                                       interactor,
                                                       log_printer)
            did_nothing = len(section_results) == 0
            for results in section_results:
                yielded_results = yielded_results or results[0]

        if did_nothing:
            interactor.did_nothing()

        if yielded_results:
            exitcode = 1